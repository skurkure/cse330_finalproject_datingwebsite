## Rubric

* Rubric turned in on time (5pts)

* Frameworks (30pts)
	* Use React.js to design the front end (10pts)
	* Use Express to design the server framework/backend (10pts)
	* Use MongoDB to implement the databases (10pts)

* Functionality (50pts)
	* Dogs can register, login, and logout from the site (5 pts)
	* Dogs are able to make a profile page that lists their details (eg. Name, breed, location, interests, qualities they look for in lovers) (15 pts)
	* Dogs are able to edit their profile (5 pts)
	* Site generates recommendations based on interests stated in profile, main page consists of listings of most compatible other dogs (15 pts)
	* Dogs can express interest in other dogs, and if the interest is mutual, they can private message (10 pts)

* Creative Portion (10pts)

* Best practices (5pts)
	* Code is well formatted and easy to read (3 pts)
	* All HTML pages pass the W3C Validator (2 pts)




