const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	username: {
		type: String,
		required: true
	},

	password: {
		type: String,
		required: true
	},

	name: {
		type: String,
		required: true
	},

	age: {
		type: Number,
		required: true
	},

	sex: {
		type: String,
		required: true
	},

	interestedSex: {
		type: String,
		required: true
	},

	location: {
		type: String,
		required: true
	},

	breed: {
		type: String,
		required: true
	},

	interests: {
		type: Array,
		required: true
	},

	bio: {
		type: String,
		required: true
	},

	interestedIn: {
		type: Array,
		required: false
	},

	interestedOthers: {
		type: Array,
		required: false
	},

	matches: {
		type: Array,
		required: false
	},

	imagePath: {
		type: String,
		required: true
	},

	alreadySeen: {
		type: Array,
		required: false
	},

	messages: {
		type: Object,
		required: true
	}

});

module.exports = User = mongoose.model('user', UserSchema);