const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const session = require('express-session');

const app = express();

const users = require('./routes/api/users');

app.use(bodyParser.json());
app.use(cors());
app.use(session({secret: "secret"}));

const db = require('./config/keys').mongoURI;

mongoose
	.connect(db)
	.then(() => console.log('MongoDB Connected...'))
	.catch(err => console.log(err));

// Use routes
app.use('/api/users', users);

app.post("/", function(req,res){
	req.session.username = req.body.username;
	console.log(req.body.username);
	res.end("asdf");
});

app.get("/username", function(req, res){
	res.json({username: req.session.username});
});

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));