import React, { Component } from 'react';
import LandingNavbar from './components/LandingNavbar';

import 'bootstrap/dist/css/bootstrap.min.css';
import './Landing.css';

class LandingPage extends Component {
    
    render() {
      return (
        <div className="Landing">
            <LandingNavbar />

        </div>
      );
    }
  }

export default LandingPage;