import React, { Component } from 'react';
import LoginForm from './components/LoginForm';

import 'bootstrap/dist/css/bootstrap.min.css';
import './Login.css';

class Login extends Component {

    render() {
      return (
        <div className="Login">
            <LoginForm />
        </div>
      );
    }
  }

export default Login;