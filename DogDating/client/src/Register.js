import RegisterNavbar from './components/RegisterNavbar';
import React, { Component } from 'react';
import { Container, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import $ from 'jquery';

import 'bootstrap/dist/css/bootstrap.min.css';
import './Register.css';

class RegisterPage extends Component {

    constructor(props){
        super(props);
        this.postForm = this.postForm.bind(this);
        this.postImage = this.postImage.bind(this);
        this.postSession = this.postSession.bind(this);
    }

    postForm(){
        let name = document.getElementById("name").value;
        let username = document.getElementById("username").value;
        let password = document.getElementById("password").value;
        let age = document.getElementById("age").value;
        let sex = $("input[name='gender']:checked").val();
        let interestedIn = $("input[name='interested']:checked").val();
        let location = document.getElementById("location").value;
        let breed = document.getElementById("breed").value;
        let bio = document.getElementById("bio").value;
        let interestsRaw = document.getElementById("interests").value;
        let interestsSpaces = interestsRaw.split(",");
        let interests = [];
        for(let i=0; i<interestsSpaces.length; i++){
            let newInterest = "";
            if(interestsSpaces[i][0] === ' '){
                for(let j=1; j<interestsSpaces[i].length; j++){
                    newInterest += interestsSpaces[i][j];
                }
                interests.push(newInterest);
            }
            else {
                interests.push(interestsSpaces[i]);
            }
        }


        if(!name || !username || !password || !age || !sex || !interestedIn || !location || !breed || !bio || !interests || !document.getElementById("uploadfile_input").files[0]){
            console.log("All fields required");
        }
        else {
            let fetchData = {
                name: name,
                username: username,
                password: password,
                age: age,
                sex: sex,
                interestedSex: interestedIn,
                location: location,
                breed: breed,
                bio: bio,
                interests: interests,
                imageName: document.getElementById('uploadfile_input').files[0].name
    
            }
            fetch("http://localhost:5000/api/users", {
                method: 'POST',
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify(fetchData)
                })
                .then(response => response.json())
                .then(data => this.postImage())
                
            }
        
    }

    postImage(){
        const form = new FormData(document.getElementById("uploadpic"));
        let username = document.getElementById("username").value;
        form.append("username", username);
        fetch('http://ec2-18-222-90-240.us-east-2.compute.amazonaws.com/ImageHosting/upload_pic.php', {
                    method: 'POST',
                    body: form
        })
        .then(response => response.json())
        .then(data => this.postSession())
    }

    postSession(){
        let sessionData = {
            username: document.getElementById("username").value
        }

        fetch("http://localhost:5000/api/users/session", {
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify(sessionData)
        })
        document.getElementById("proceed").click();
    }
    
    render() {
      return (
        <div id='background'>
            <RegisterNavbar/>
            <div className="Register">
                <br/>
                <br/>
                <div id='register_header'>
                    <h2>Sign Up</h2>
                </div>
                <Container>
                    <Form>
                        <br/>
                        <Col>
                            <FormGroup className="form_group">
                                <Label>Full Name</Label>
                                <Input type="text" name='name' id='name' placeholder='John Smith' />
                            </FormGroup>
                            <FormGroup className="form_group">
                                <Label>Username</Label>
                                <Input type="text" name='username' id='username' placeholder='Username' />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup className="form_group">
                                <Label>Password</Label>
                                <Input type="password" name='password' id='password' placeholder='***********' />
                            </FormGroup>
                            <FormGroup className="form_group">
                                <Label>Age</Label>
                                <Input type="text" name='age' id='age'/>
                            </FormGroup>
                            <FormGroup className="form_group">
                                    <span>Sex: &nbsp; &nbsp;</span>
                                    <input type="radio" name='gender' value='male'/> Male &nbsp; &nbsp;
                                    <input type='radio' name='gender' value='female'/> Female
                            </FormGroup>
                            <FormGroup className="form_group">
                                    <span>Interested in: &nbsp; &nbsp;</span>
                                    <input type="radio" name='interested' value='male'/> Males &nbsp; &nbsp;
                                    <input type='radio' name='interested' value='female'/> Females
                            </FormGroup>
                            <FormGroup className="form_group">
                                <Label>Breed</Label>
                                <Input type="text" name='breed' id='breed' placeholder='Labrador Retriever' />
                            </FormGroup>
                            <FormGroup className="form_group">
                                <Label>Location</Label>
                                <Input type="text" name='location' id='location' placeholder='Los Angeles, CA'/>
                            </FormGroup>
                            <FormGroup className="form_group">
                                <Label>Interests (separated by commas)</Label><br/>
                                <textarea rows='2' cols='30' id='interests' placeholder='hiking, swimming, playing fetch'></textarea>
                            </FormGroup>
                            <FormGroup className="form_group">
                                <Label>Bio:</Label><br/>
                                <textarea rows='3' cols='30' id='bio' placeholder="A siberian husky with a sophisticated side"></textarea>
                            </FormGroup>
                        </Col>
                    </Form>

                    <form method='post' action='http://localhost:5000/upload_pic.php' id='uploadpic'>
                        <Label>Upload a profile picture</Label>
                        <br/>
                        <input name="uploadedfile" type="file" id="uploadfile_input" />
                    </form>
                    <br/>
                    <div id='submitButtonDiv'>
                        <Button color="info" id='submitButton' onClick={this.postForm}>Submit</Button>
                    </div>

                    <br/>
                    <div id='message'></div>
                    <a href='/home' id='proceed'></a>
                </Container>

            </div>
        </div>
      );
    }
  }

export default RegisterPage;