import React, { Component } from 'react';
import { Container, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { createHashHistory } from 'history';
export const history = createHashHistory();

class LoginForm extends Component{
    constructor(props){
        super(props);
        this.loginAuth = this.loginAuth.bind(this);
        this.authenticate = this.authenticate.bind(this);
        this.state = {isAuthenticated: false}
    }

    loginAuth(){
        let username = document.getElementById("username").value;
        let password = document.getElementById("password").value;

        let fetchData = {
            username: username,
            password: password
        }

        fetch("http://localhost:5000/api/users/authenticate", {
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify(fetchData)
        })
        .then(response => response.json())
        .then(data => this.authenticate(data))
    }

    authenticate(data){
        if(!data.authenticated){
            document.getElementById("message").innerText = "Invalid credentials";
        }
        else {
            let username = document.getElementById("username").value;
            let sessionData = {
                username: username
            }

            fetch("http://localhost:5000/api/users/session", {
                method: 'POST',
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify(sessionData)
            })
            document.getElementById("proceed").click();
        }
    }

    render() {
        return (
            <div className='loginForm'>
                <Container>
                    <div id='loginHeader'>
                        <br/>
                    </div>
                    <Form className="form" method="post">
                        <div id='logo'>
                            <img src={require('./paws.png')} alt='' width='80' height='80'/>    
                        </div>  
                        <br/>
                        <Col>
                            <FormGroup>
                                <Label>Username</Label>
                                <Input type="text" name='username' id='username' placeholder='Username' />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label>Password</Label>
                                <Input type="password" name='password' id='password' placeholder='***********' />
                            </FormGroup>
                        </Col>
                    </Form>
                    <br/>
                    <div id='submitButtonDiv'>
                        <Button color="info" id='submitButton' onClick={this.loginAuth}>Submit</Button>
                    </div>
                    <br/>
                    <div id='message'></div>
                    <a href='/home' id='proceed'></a>
                </Container>
            </div>
        );
        
    }

}

export default LoginForm;