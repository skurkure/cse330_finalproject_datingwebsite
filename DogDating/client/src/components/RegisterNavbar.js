import React, { Component } from 'react';
import {
    Navbar,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Container
} from 'reactstrap';

import {Link} from 'react-router-dom';

class RegisterNavbar extends Component {
    
    render() {
        return (
            <div>
                <Navbar color="dark" dark expand="sm" className="mb-5">
                    <Container>
                        <NavbarBrand href='/'>DoggyDating</NavbarBrand>
                            <Nav className="ml-auto" navbar>
                                <NavItem>
                                    <NavLink tag={Link} to='/'>Cancel Registration</NavLink>
                                </NavItem>
                            </Nav>
                    </Container>
                </Navbar>
            </div>
        );
        
    }
    
}

export default RegisterNavbar;