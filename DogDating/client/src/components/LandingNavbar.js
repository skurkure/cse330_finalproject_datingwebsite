import React, { Component } from 'react';
import {
    Navbar,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Container
} from 'reactstrap';

class LandingNavbar extends Component {
    
    render() {
        return (
            <div>
                <Navbar color="dark" dark expand="sm" className="mb-5">
                    <Container>
                        <NavbarBrand href='/'>DoggyDating</NavbarBrand>
                            <Nav className="ml-auto" navbar>
                                <NavItem>
                                    <NavLink href='/login'>Log In</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink href='/register'>Sign Up</NavLink>
                                </NavItem>
                            </Nav>
                    </Container>
                </Navbar>
            </div>
        );
        
    }
    
}

export default LandingNavbar;