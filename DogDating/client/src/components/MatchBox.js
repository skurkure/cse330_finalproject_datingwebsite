import React, { Component, Fragment } from 'react';
import { Row, Col, Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button, CardLink, Modal, ModalBody, ModalHeader, ModalFooter, Input, Label } from 'reactstrap';
import {Link} from 'react-router-dom';
import Fade from 'react-fade-opacity';

class MatchBox extends Component {
    constructor(props){
        super(props);

        let interestsStr = "";
        for(let i=0; i<this.props.userData.interests.length; i++){
            if(i < this.props.userData.interests.length - 1){
                interestsStr += this.props.userData.interests[i] + ", ";
            }
            else {
                interestsStr += this.props.userData.interests[i];
            }
        }

        this.state = {
            modal: false,
            username: this.props.userData.username,
            interestsStr: interestsStr
        }

        this.toggle = this.toggle.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
    }

    

    toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal,
          username: prevState.username,
          interestsStr: prevState.interestsStr
        }));
    }

    sendMessage(){
        let message = document.getElementById("newmessage").value;
        let fetchData = {
            recipient: this.state.username,
            message: message
        }
        fetch("http://localhost:5000/api/users/sendmessage", {
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify(fetchData)
        })
        .then(response => response.json())
        .then(data => this.toggle())

    } 

    render(){
        return(
            <Fragment>
            <Col className="userbox" xs="3">
                <Card>
                    <CardImg src={this.props.userData.imagePath}/>
                    <CardBody>
                        <CardTitle className="name"><strong>{this.props.userData.name}, {this.props.userData.age}</strong></CardTitle>
                        <CardSubtitle>{this.props.userData.location}</CardSubtitle>
                        <br/>
                        <Link href="#" onClick={this.toggle}></Link>
                        <Button outline color="info" className="viewProfile" onClick={this.toggle}>View Profile</Button>
                    </CardBody>
                </Card>
            </Col>

            <Modal isOpen={this.state.modal} toggle={this.toggle}>
                <ModalHeader toggle={this.toggle}>{this.props.userData.name}</ModalHeader>
                <ModalBody>
                    <img src={this.props.userData.imagePath} height="200" width="230" id="profileimage"/>
                    <br/>
                    <br/>
                    <h5>{this.props.userData.bio}</h5>
                    <hr className="my-2" />
                    <p>Age: {this.props.userData.age}</p>
                    <p>Location: {this.props.userData.location}</p>
                    <p>Interests: {this.state.interestsStr}</p>
                    <p>Breed: {this.props.userData.breed}</p>
                    <br/>
                    <Label>Send Message:</Label>
                    <Input type="textarea" id="newmessage"></Input>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={this.toggle}>Close</Button>
                    <Button color="info" onClick={this.sendMessage}>Send Message</Button>
                </ModalFooter>
            </Modal>
            </Fragment>
        )
    }
}

export default MatchBox;