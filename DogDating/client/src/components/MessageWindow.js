import React, { Component, Fragment } from 'react';
import { Row, Col, Card, CardImg, CardBody, CardTitle, CardSubtitle, Button, CardLink, Modal, ModalBody, ModalHeader, ModalFooter, Input, Label } from 'reactstrap';

class MessageWindow extends Component {
    constructor(props){
        super(props);
        this.state = {
            modal: false,
            conversation: this.props.conversation
        }
        this.toggle = this.toggle.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.initialState = this.initialState.bind(this);
        this.updateMessage = this.updateMessage.bind(this);
        
    }

    componentDidMount(){
        fetch("http://localhost:5000/api/users/session", {
            method: 'GET',
            headers: { 'content-type': 'application/json' }
        })
        .then(response => response.json())
        .then(data => this.initialState(data))
    }

    initialState(data){
        this.setState(prevState => ({
            modal: prevState.modal,
            username: data.username,
            conversation: this.props.conversation
       })) 
    }

    toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal,
          conversation: prevState.conversation,
          username: prevState.username
        }));
    }


    sendMessage(){
        let message = document.getElementById("newmessage").value;
        let fetchData = {
            recipient: this.props.user,
            message: message
        }
        fetch("http://localhost:5000/api/users/sendmessage", {
            method: 'POST',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify(fetchData)
        })
        .then(response => response.json())
        .then(data => this.updateMessage(message))

    } 

    updateMessage(message){
        let currConversation = this.state.conversation;
        currConversation.push([this.state.username, message]);
        console.log(currConversation);
        this.setState(prevState => ({
            conversation: currConversation,
            modal: prevState.modal,
            username: prevState.username
        }))
    }

    render(){
        return(
            <Fragment>
                <Col className="userbox" xs="3">
                    <Card>
                        <CardBody>
                            <CardTitle className="name"><strong>{this.props.user}</strong></CardTitle>
                            <br/>
                            <Button outline color="info" className="viewProfile" onClick={this.toggle}>View Conversation</Button>
                        </CardBody>
                    </Card>
                </Col>
                {/* <p>{this.props.user}</p>
                <Button onClick={this.toggle}>View Conversation</Button> */}
                <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}>{this.props.user}</ModalHeader>
                    <ModalBody>
                        {this.props.conversation.map((element) => {
                            return <p><strong>{element[0]}:</strong> {element[1]}</p>
                        })}
                        <br/>
                        <Label>New Message:</Label>
                        <Input type="textarea" id="newmessage"></Input>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="info" onClick={this.sendMessage}>Send Message</Button>
                        <Button color="secondary" onClick={this.toggle}>Close</Button>
                    </ModalFooter>
                </Modal>
            </Fragment>
        )
    }
}

export default MessageWindow;