import React, { Component, Fragment } from 'react';
import { Row, Col, Card, CardImg, CardBody, CardTitle, CardSubtitle, Button, CardLink, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';
import Fade from 'react-fade-opacity';

class UserBox extends Component {
    constructor(props){
        super(props);
        this.toggle = this.toggle.bind(this);
        let interestsStr = "";
        for(let i=0; i<this.props.userData.interests.length; i++){
            if(i < this.props.userData.interests.length - 1){
                interestsStr += this.props.userData.interests[i] + ", ";
            }
            else {
                interestsStr += this.props.userData.interests[i];
            }
        }

        this.state = {
            modal: false,
            interestsStr: interestsStr
        }
    }

    toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal,
          interestsStr: prevState.interestsStr
        }));
    }

    render(){
        return(
            <Fragment>
            <Col className="userbox" xs="3">
                <Card>
                    <CardImg src={this.props.userData.imagePath}/>
                    <CardBody>
                        <CardTitle className="name"><strong>{this.props.userData.name}, {this.props.userData.age}</strong></CardTitle>
                        <CardSubtitle>{this.props.userData.location}</CardSubtitle>
                        <CardLink href="#" className='interest' onClick={() => this.props.clickHandler(1, this.props.userData)}>Interested</CardLink>
                        <CardLink href="#" className='interest' onClick={() => this.props.clickHandler(0, this.props.userData)}>Not Interested</CardLink>
                        <br/>
                        <Button outline color="info" className="viewProfile" onClick={this.toggle}>View Profile</Button>
                    </CardBody>
                </Card>
            </Col>
            
                <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}>{this.props.userData.name}</ModalHeader>
                    <ModalBody>
                        <img src={this.props.userData.imagePath} height="200" width="230"/>
                        <br/>
                        <br/>
                        <h5>{this.props.userData.bio}</h5>
                        <hr className="my-2" />
                        <p>Age: {this.props.userData.age}</p>
                        <p>Location: {this.props.userData.location}</p>
                        <p>Interests: {this.state.interestsStr}</p>
                        <p>Breed: {this.props.userData.breed}</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle}>Close</Button>
                    </ModalFooter>
                </Modal>
            </Fragment>
        )
    }
}

export default UserBox;