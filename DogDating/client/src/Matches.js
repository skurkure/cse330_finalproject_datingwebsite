import React, { Component } from 'react';
import AppNavbar from './components/AppNavbar';
import { Button, Row, Container, CardDeck } from 'reactstrap';
import MatchBox from './components/MatchBox';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Home.css';

class MatchesPage extends Component {
    constructor(props){
        super(props);
        this.state = {};
        this.state.matches = [];
        this.updateMatches = this.updateMatches.bind(this);
    }

    componentDidMount(){
        fetch("http://localhost:5000/api/users/getMatches", {
            method: 'GET',
            headers: { 'content-type': 'application/json' },
        })
        .then(response => response.json())
        .then(data => this.updateMatches(data));
    }

    updateMatches(data){
        this.setState({matches: data.matches})
    }
    

    render(){
        if(this.state.matches.length == 0){
            return(
                <div className="Home">
                    <AppNavbar/>
                    <div id='page_header'>
                        <h2>Your Matches</h2>
                        <br/>
                        <br/>
                        <h3>You have no matches...</h3>
                    </div>
                </div>
            )
        }
        else {
            return(
                <div className="Matches">
                    <AppNavbar/>
                    <div id='page_header'>
                        <h2>Your Matches</h2>
                    </div>
                    <br/>
    
                    <Container>
                        <CardDeck>
                            {this.state.matches.map((user) => {
                                return (
                                    <MatchBox userData={user}/>
                                )
                            })}
                        </CardDeck>
                    </Container>
                </div>
            )
        }
    }
}

export default MatchesPage;