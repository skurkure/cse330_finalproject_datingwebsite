import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import LoginPage from './Login.js';
import HomePage from './Home.js';
import LandingPage from './Landing.js';
import RegisterPage from './Register.js';
import MyProfile from "./MyProfile.js";
import MatchesPage from "./Matches.js";
import MessagePage from './Messages.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
 

class App extends Component {
  render() {
    return (
      <div className="App">
        {/* <Route eact path="/" render={() => <Redirect to="/login" />} /> */}
        <Route exact path="/" component={LandingPage}/>
        
        <Route exact path="/home" component={HomePage}/> 
        <Route exact path="/login" component={LoginPage}/>
        <Route exact path="/register" component={RegisterPage}/>
        <Route exact path="/profile" component={MyProfile}/>
        <Route exact path="/matches" component={MatchesPage}/>
        <Route exact path="/messages" component={MessagePage}/>


        {/* <Redirect from='/login/home' to='/home'/> */}


      </div>
    );
  }
}

export default App;
