import React, { Component } from 'react';
import "./Messages.css";
import AppNavbar from "./components/AppNavbar";
import {Button, CardDeck, Container} from 'reactstrap';
import MessageWindow from "./components/MessageWindow";

class MessagePage extends Component {
    constructor(props){
        super(props);
        this.state = {
            messages: {}
        }
        this.getState = this.getState.bind(this);
        this.updateMessages = this.updateMessages.bind(this);

    }

    componentDidMount(){
        fetch("http://localhost:5000/api/users/getmessages", {
            method: 'GET',
            headers: { 'content-type': 'application/json' },
        })
        .then(response => response.json())
        .then(data => this.updateMessages(data))
    }

    getState(){
        console.log(this.state);
    }

    updateMessages(data){
        delete data.dummy;
        this.setState({messages: data})
    }

    render(){
        if(Object.keys(this.state.messages).length == 0){
            return(
                <div>
                <AppNavbar/>
                    <div id="page_header">
                        <h2>Messages</h2>
                        <br/>
                        <br/>
                        <h4>You have no messages...</h4>
                    </div>
                </div>
            )
        }
        return(
            <div>
                <AppNavbar/>
                <div id="page_header">
                    <h2>Messages</h2>
                    <br/>
                </div>

                <Container>
                    <CardDeck>
                        {Object.keys(this.state.messages).map((key) => {
                            return <MessageWindow user={key} conversation={this.state.messages[key]}/>
                        })}
                    </CardDeck>
                </Container>
            </div>
        )
    }
}

export default MessagePage;