import React, { Component, Image } from 'react';
import { Button, Modal, FormGroup, Label, Input, ModalHeader, ModalFooter} from 'reactstrap';
import AppNavbar from "./components/AppNavbar";

import 'bootstrap/dist/css/bootstrap.min.css';
import './MyProfile.css';

class MyProfile extends Component {
    constructor(props){
        super(props);
        this.state = {
            userData: {},
            modal: false
        }
        this.showState = this.showState.bind(this);
        this.stateUpdate = this.stateUpdate.bind(this);
        this.toggle = this.toggle.bind(this);
        this.updateProfile = this.updateProfile.bind(this);
        this.stringifyInterests = this.stringifyInterests.bind(this);


    }

    componentDidMount(){
        fetch('http://localhost:5000/api/users/mydata', {
            method: "GET",
            headers: { 'content-type': 'application/json' }
        })
        .then(response => response.json())
        .then(data => this.stateUpdate(data))


    }

    stateUpdate(data){
        this.setState({userData: data}, () => {
            let interestsStr = "";
            for(let i=0; i<this.state.userData.interests.length; i++){
                if(i < this.state.userData.interests.length - 1){
                    interestsStr += this.state.userData.interests[i] + ", ";
                }
                else {
                    interestsStr += this.state.userData.interests[i];
                }
            }
            let newData = this.state.userData;
            newData['interestsStr'] = interestsStr;
            this.setState({userData: newData});
        });
    }

    showState(){
        console.log(this.state);
    }

    toggle() {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
    }

    updateProfile(){
        let newBio = document.getElementById("newbio").value;
        let newInterests = document.getElementById("newinterests").value;
        let newLocation = document.getElementById("newlocation").value;
        let interestsSpaces = newInterests.split(",");
        let interests = [];
        for(let i=0; i<interestsSpaces.length; i++){
            let newInterest = "";
            if(interestsSpaces[i][0] === ' '){
                for(let j=1; j<interestsSpaces[i].length; j++){
                    newInterest += interestsSpaces[i][j];
                }
                interests.push(newInterest);
            }
            else {
                interests.push(interestsSpaces[i]);
            }
        }

        console.log(interests);

        let fetchData = {
            bio: newBio,
            location: newLocation,
            interests: interests
        }

        fetch('http://localhost:5000/api/users/updateUser', {
            method: "POST",
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify(fetchData)
        })
        .then(response => response.json())
        .then(data => this.stringifyInterests(data))

    }

    stringifyInterests(data){
        let interests = data.interests;
        let interestsStr = "";
        let newData = data;
        for(let i=0; i<interests.length; i++){
            if(i < interests.length - 1){
                interestsStr += interests[i] + ", ";
            }
            else {
                interestsStr += interests[i];
            }
        }

        newData['interestsStr'] = interestsStr;
        this.setState(prevState => ({
            modal: !prevState.modal,
            userData: newData
        }));
    }


    render(){
        return(
            <div className="maincontent">
                <AppNavbar/>
                <div className="profileDetails">
                    <h3>{this.state.userData.name}</h3>
                    <img src={this.state.userData.imagePath} width='240' height='218' id='img'/>
                    <br/>
                    <br/>
                    <p className="lead">{this.state.userData.bio}</p>
                    <hr className="my-2" />
                    <p>Age: {this.state.userData.age}</p>
                    <p>Location: {this.state.userData.location}</p>
                    <p>Interests: {this.state.userData.interestsStr}</p>
                    <p>Breed: {this.state.userData.breed}</p>
                    <br/>
                    <Button color="info" onClick={this.toggle}>Edit Profile</Button>
                </div>
                <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}>Edit Profile</ModalHeader>
                    <br/>
                    <FormGroup>
                        <Label id="biolabel">Bio</Label>
                        <Input type="textarea" id='newbio' defaultValue={this.state.userData.bio} />
                    </FormGroup>
                    <FormGroup>
                        <Label id="locationlabel">Location</Label>
                        <Input type="text" id='newlocation' placeholder={this.state.userData.location} />
                    </FormGroup>
                    <FormGroup>
                        <Label id="interestlabel">Interests</Label>
                        <Input type="text" id='newinterests' defaultValue={this.state.userData.interestsStr} />
                    </FormGroup>
                    <br/>
                    <ModalFooter>
                        <Button color="info" onClick={this.updateProfile}>Update Profile</Button>{' '}
                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

export default MyProfile;