import React, { Component } from 'react';
import AppNavbar from './components/AppNavbar';
import { Button, Row, Container, CardDeck } from 'reactstrap';
import UserBox from './components/UserBox';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Home.css';

class HomePage extends Component {

    constructor(props){
        super(props);
        this.getSession = this.getSession.bind(this);
        this.getRecs = this.getRecs.bind(this);
        this.printState = this.printState.bind(this);
        this.clickHandler = this.clickHandler.bind(this);
        this.state = {};
        this.state.recommendations = [];
    }

    componentDidMount(){
        const that = this;
        this.getSession(that);
    }

    getSession(that){
        fetch('http://localhost:5000/api/users/session', {
            method: "GET",
            headers: { 'content-type': 'application/json' }
        })
        .then(response => response.json())
        .then(data => this.getRecs(that));
    }

    getRecs(that){
        fetch('http://localhost:5000/api/users/recommended', {
            method: "GET",
            headers: { 'content-type': 'application/json' }
        })
        .then(response => response.json())
        .then(data => {that.setState({recommendations: data.recommendations})})
    }

    printState(){
        console.log(this.state);
        console.log("asdf");
    }

    clickHandler(interest, userData){
        let removeIndex = 0;
        let currRecommendations = this.state.recommendations;
        var newRecommendations;
        for(var i=0; i<currRecommendations.length; i++){
            if(currRecommendations[i].username == userData.username){
                removeIndex = i;
            }
        }
        currRecommendations.splice(removeIndex, 1);
        newRecommendations = currRecommendations;
        console.log(interest);
        this.setState({recommendations: newRecommendations});
        //Need to add this user to already seen for current user
        if(interest == 1){
            fetch('http://localhost:5000/api/users/interest', {
                method: "POST",
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify({user: userData})
            })
            .then(response => response.json())
            .then(data => console.log(data));
        }
        else {
            fetch('http://localhost:5000/api/users/notinterest', {
                method: "POST",
                headers: { 'content-type': 'application/json' },
                body: JSON.stringify({user: userData})
            })
            .then(response => response.json())
            .then(data => console.log(data));
        }

    }

    render() {
      return (
        <div className="Home">
            <AppNavbar/>
            <div id='page_header'>
                <h2>Recommendations</h2>
            </div>
            <br/>

            <Container>
                <CardDeck>
                    {this.state.recommendations.map((user) => {
                        return (
                            <UserBox userData={user} clickHandler={this.clickHandler}/>
                        )
                    })}
                </CardDeck>
            </Container>
        </div>
      );
    }
  }

export default HomePage;