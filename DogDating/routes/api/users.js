const express = require('express');
const router = express.Router();
const fs = require('fs');
const file = require('save-file');
var FileReader = require('filereader');
fileReader = new FileReader();
const session = require("express-session");

const User = require('../../models/user');

var loggedInUser;

// @route GET /api/users
// @desc Get all users

router.get('/', (req, res) => {
	console.log("requested");
	User.find()
		.then(items => res.json(items));
});


// @route POST /api/users
// @desc Add a user

router.post('/', (req, res) => {
	// //Make the image path based on username and image name, and save the image
	// let imageObj = req.body.image;
	
	let imageName = req.body.imageName;
	let username = req.body.username;
	let imagePath = "http://ec2-18-222-90-240.us-east-2.compute.amazonaws.com/ImageHosting/" + username + "/" + imageName;
	// console.log(imageObj.name);
	// let username = req.body.username;
	// let dirPath = "/etc/DoggyPics/"+username;
	// if (!fs.existsSync(dirPath)){
	// 	fs.mkdirSync(dirPath);
	// }
	// let imagePath = dirPath + "/" + imageName;
	// console.log(imagePath);
	// fs.writeFile(imagePath, req.body.image, function(err){
	// 	if(err){
	// 		console.log(err);
	// 	}
	// 	else {
	// 		console.log("success");
			
	// 	}
	// })



	const newUser = new User({
		username: req.body.username,
		password: req.body.password,
		name: req.body.name,
		age: req.body.age,
		sex: req.body.sex,
		interestedSex: req.body.interestedSex,
		location: req.body.location,
		breed: req.body.breed,
		bio: req.body.bio,
		interests: req.body.interests,
		interestedIn: [],
		interestedOthers: [],
		matches: [],
		imagePath: imagePath,
		messages: {"dummy": "message"}

	});

	console.log(newUser);

	newUser.save().then(user => res.json(user));
});


// @route POST /api/users/authenticate
// @desc Authenticate username and password
router.post('/authenticate', (req, res) => {
	let username = req.body.username;
	let password = req.body.password;

	User.find({username: username}, function(err, items){
		if(items.length == 0){
			res.json({message: false});
		}
		else {
			let storedPass = items[0]['password'];
			if(storedPass != password){
				res.json({authenticated: false});
			}
			else {
				res.json({authenticated: true});
			}
		}
	});

});

router.get("/recommended", (req, res) => {
	var recommendationMap = new Map([]);
	let recommendations = {};

	User.find({username: loggedInUser}, function(err, items){
		var currUser = items[0];
		var interests = new Set(items[0]['interests']);
		User.find({}, function(err, otherItems){
			otherItems.forEach((user) => {
				if(user['username'] != loggedInUser){
					if(currUser['interestedSex'] == user['sex']){
						let alreadySeen = new Set(currUser.alreadySeen);
						if(!alreadySeen.has(user.username)){
							let score = 0;
							let currInterests = user['interests'];
							currInterests.forEach((interest) => {
								if(interests.has(interest)){
									score += 1;
								}
							});
							if(!recommendationMap.get(score)){
								recommendationMap.set(score, [user]);
							}
							else {
								recommendationMap.get(score).push(user);
							}
						}
					}	
				}
			})
				
			
			let sortedScores = Array.from(recommendationMap.keys()).sort();
			var unique = sortedScores.filter(function(item, index){
				return sortedScores.indexOf(item) >= index;
			});

			let response = {};
			let returnedUsers = [];

			unique.forEach((score) => {
				var currUsers = recommendationMap.get(score);
				currUsers.forEach((user) => {
					returnedUsers.push(user);
				})
			})

			returnedUsers.reverse();

			response['recommendations'] = returnedUsers;

			console.log(returnedUsers[0]);

			res.json(response);

		});
	});
});

router.post("/interest", (req, res) => {
	let otherUser = req.body.user;
	let otherUsername = otherUser.username;
	User.find({username: loggedInUser}, function(err, items){
		let currUser = items[0];
		User.find({username: otherUsername}, function(err, otherItems){
			let isMatch = false;
			let otherUserData = otherItems[0];
			let otherUserInterested = otherUserData.interestedIn;
			otherUserInterested.forEach((user) => {
				if(user.username == currUser.username){
					isMatch = true;
				}
			});

			let currUserNew = {
				username: currUser.username,
				name: currUser.name,
				imagePath: currUser.imagePath,
				age: currUser.age,
				breed: currUser.breed,
				bio: currUser.bio,
				interests: currUser.interests,
				location: currUser.location
			}

			let otherUserNew = {
				username: otherUserData.username,
				name: otherUserData.name,
				imagePath: otherUserData.imagePath,
				age: otherUserData.age,
				breed: otherUserData.breed,
				bio: otherUserData.bio,
				interests: otherUserData.interests,
				location: otherUserData.location
			}
			
			if(isMatch){
				otherUserData.matches.push(currUserNew);
				currUser.matches.push(otherUserNew);
			}
			else {
				currUser.interestedIn.push(otherUserData);
			}

			currUser.alreadySeen.push(otherUsername);
			console.log(currUser.alreadySeen);

			User.findOneAndUpdate({username: loggedInUser}, currUser, function(err, update){
				User.findOneAndUpdate({username: otherUsername}, otherUserData, function(othererr, newupdate){
					res.json({success: true});
				});
			})
		})
	})
})

router.post('/sendmessage', (req, res) => {
	let recipient = req.body.recipient;
	let message = req.body.message;

	User.find({username: loggedInUser}, function(err, items){
		let currUser = items[0];
		console.log(currUser);
		if(currUser.messages[recipient] == undefined){
			currUser.messages[recipient] = [[loggedInUser, message]];
		}
		else {
			currUser.messages[recipient].push([loggedInUser, message]);
		}

		User.findOneAndUpdate({username: loggedInUser}, currUser, {new: true}, function(err, update){
			User.find({username: recipient}, function(err, otherItems){
				let otherUser = otherItems[0];
				console.log(otherUser);
				if(otherUser.messages[loggedInUser] == undefined){
					otherUser.messages[loggedInUser] = [[loggedInUser, message]];
				}
				else {
					otherUser.messages[loggedInUser].push([loggedInUser, message]);
				}
				
				User.findOneAndUpdate({username: recipient}, otherUser, {new: true}, function(err, otherUpdate){
					res.json({success: true});
				})
			})
		})

	})
})

router.get("/getmessages", (req, res) => {
	User.find({username: loggedInUser}, function(err, items){
		let currUser = items[0];
		let messages = currUser.messages;
		res.json(messages);
	})
})

router.post("/notinterest", (req, res) => {
	let otherUser = req.body.user;
	let otherUsername = otherUser.username;

	User.find({username: loggedInUser}, function(err, items){
		let currUser = items[0];
		currUser.alreadySeen.push(otherUsername);
		User.findOneAndUpdate({username: loggedInUser}, currUser, function(err, update){
			res.json({success: true});
		});
	});
})

router.get("/mydata", (req, res) => {
	User.find({username: loggedInUser}, function(err, items){
		let currUser = items[0];
		res.json(currUser);
	});
});

router.post('/session', (req, res) => {
	loggedInUser = req.body.username;
	res.json({loggedIn: true});
});

router.get('/session', (req, res) => {
	res.json({username: loggedInUser});
})

router.post("/updateUser", (req, res) => {
	let newBio = req.body.bio;
	let newInterests = req.body.interests;
	let newLocation = req.body.location;

	User.find({username: loggedInUser}, function(err, items){
		let currUser = items[0];
		currUser.interests = newInterests;
		currUser.bio = newBio;
		currUser.location = newLocation;
		User.findOneAndUpdate({username: loggedInUser}, currUser, {new: true}, function(err, update){
			res.json(update);
		});
	});

})

router.get("/getMatches", (req, res) => {
	User.find({username: loggedInUser}, function(err, items){
		let currUser = items[0];
		let matches = currUser.matches;
		res.json({matches: matches});
	})
})


module.exports = router;